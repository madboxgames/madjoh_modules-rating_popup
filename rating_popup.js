define([
	'require',
	'madjoh_modules/wording/wording'
],
function(require, Wording){
	var RatingPopup = {
		init : function(settings){
			if(!window.cordova) return;

			AppRate.preferences = {
				openStoreInApp : true,
				displayAppName : Wording.getText(settings.displayAppName),
				usesUntilPrompt : 5,
				promptAgainForEachNewVersion : false,
				storeAppURL : {
					ios 	: settings.storeAppURLIOS,
					android : settings.storeAppURLAndroid,
				},
				customLocale : {
					title 				: Wording.getText(settings.title),
					message 			: Wording.getText(settings.message),
					cancelButtonLabel 	: Wording.getText(settings.cancelButtonLabel),
					laterButtonLabel 	: Wording.getText(settings.laterButtonLabel),
					rateButtonLabel 	: Wording.getText(settings.rateButtonLabel)
				}
			};

			setTimeout(function(){AppRate.promptForRating(false);}, 5000);
		}
	};

	return RatingPopup;
});
